@extends('layouts.app')

@section('title', 'Health Science Databases')

@section('content')
    <div class="browse_links">
        Browse Databases :
        @foreach (array_keys($resources) as $browseLetter)
        <a href="#{{$browseLetter}}" target="_self" style="text-decoration:none;color:#003366;">&nbsp;{{$browseLetter}}&nbsp;</a>
        @endforeach
    </div>
    <table>
        @foreach (array_keys($resources) as $browseLetter)
            <tr>
                <td>
                    <a id="{{$browseLetter}}" name="{{$browseLetter}}">
                        <h4>{{$browseLetter}}</h4>
                    </a>
                </td>
            </tr>
            <tr class="table-header">
                <td class="title">Title</th>
                <td>Source</th>
            </tr>
            @foreach ($resources[$browseLetter] as $resource)
                <tr>
                    <td class="data title"><a class='link' href="https://databases.library.arizona.edu/dbfind.php?shortname={{$resource['short_name']}}">{{$resource['title']}}</a></td>
                    <td class="data">{{$resource['source']}}</td>
                </tr>
            @endforeach
        @endforeach
    </table

@endsection