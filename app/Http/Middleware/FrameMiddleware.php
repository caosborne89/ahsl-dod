<?php

namespace App\Http\Middleware;

use Closure;

class FrameMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('X-Frame-Options', 'ALLOW FROM http://ahsl.arizona.edu');
        return $response;
    }
}
