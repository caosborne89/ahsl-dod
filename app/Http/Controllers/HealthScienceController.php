<?php

namespace App\Http\Controllers;

class HealthScienceController extends Controller
{
    public $categories;
    protected $subjectID;

    public function __construct()
    {
        /* Subject ID for Public Health */
        $this->subjectID = 247;

        $this->getCategories();
    }

    private function getCategories()
    {
        $this->categories = [];

        $query = "SELECT category_id, subject_id, category_name, category_order, DESCRIPTION FROM category WHERE subject_id = '" . $this->subjectID . "' ORDER BY category_order";
        $results = app('db')->select($query);

        foreach($results as $result) {
            $this->categories[$result->category_name] = $result->category_id;
        }
    }

    public function getCategoryResources($categoryID)
    {
        $query = "SELECT DISTINCT resource_category_xref.resource_id, short_name, title, publisher.name AS source FROM resource_category_xref ";
        $query .= "INNER JOIN resource ON resource_category_xref.resource_id = resource.resource_id INNER JOIN title on resource_category_xref.resource_id=title.resource_id INNER JOIN publisher ON resource.publisher_id=publisher.publisher_id ";
        $query .= "WHERE resource_category_xref.category_id = '" . $categoryID . "' AND resource.release_flag =1 AND title.title_order=0 ORDER BY title";
        $results = app('db')->select($query);

        return $results;
    }

    public function getAllResources() 
    {
        $allResources = [];
        foreach($this->categories as $categoryID) {
            $results = $this->getCategoryResources($categoryID);
            $allResources = array_merge($allResources, $results);
        }

        $allFormattedResources = $this->reformatResources($allResources);
        ksort($allFormattedResources);
        $resourcesSectionedAZ = $this->formatAZList($allFormattedResources);

        return view('allResources', ['resources' => $resourcesSectionedAZ]);
    }

    protected function reformatResources($resourceArr)
    {
        $reformatedArr = [];
        foreach($resourceArr as $resource) {
            $title = $resource->title;
            $meta_data = [];
            foreach ($resource as $key => $value) {
                $meta_data[$key] = $value;
            }
            $reformatedArr[$title] = $meta_data;
        }
        return $reformatedArr;
    }

    protected function formatAZList($resources) 
    {
        $azArr = [];
        $currLetter = "#";
        foreach($resources as $resource) {
            $resourceFirstLetter = $resource['title'][0];
            if(!ctype_alpha($resourceFirstLetter)) {
                $resourceFirstLetter = "#";
            }
            if($resourceFirstLetter !== $currLetter) {
                $currLetter = $resourceFirstLetter;
            }
            $azArr[$currLetter][$resource['title']] = $resource;
        }
        return $azArr;
    }

}